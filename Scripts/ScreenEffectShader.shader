﻿Shader "ScreenEffect/ScreenEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			//Pragma for frag and vert functions
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			//Struct for appdata
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			//Struct for vert to fragment
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			//Reference for Parmeter to get the maintexture
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				// Set Color to the main texture.
				// But Distort the location where the Main texture is being sampled from.
				fixed4 col = tex2D(_MainTex, i.uv + float2(sin(i.vertex.y/50+ _Time[1])/50,sin(i.vertex.x / 50 + _Time[1]) / 50));
				return col;
			}
			ENDCG
		}
	}
}
