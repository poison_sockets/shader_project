﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateModel : MonoBehaviour {

    public float speed;
    public Vector3 rotation = new Vector3(0, 0, 0);

    public GameObject model;
    Transform modelTransform;
    bool canRotate;

    const float defualtSpeed = 10;
    const float defaultRotation = 10;

	// Use this for initialization
	void Start () {

      
        //setting defualt values of rotation and speed if none have been set.
        if (speed == 0 && rotation.y == 0)
        {

            
            speed = defualtSpeed;
            rotation.y = defaultRotation;

        }




        if(model != null)
        {
            // IF a model ASSIGNED set can rotate to true and assign model transform to model.transform
            canRotate = true;
            modelTransform = model.transform;

        }
        else
        {
            // if no model is assigned assign model to the gameobject that this script is attached to 
            // assign modelTransform to model.transform
            // finally set canRotate to true
            model = transform.gameObject;
            modelTransform = model.transform;
            canRotate = true;
  

        }

	}
	
	// Update is called once per frame
	void Update () {

        if (canRotate)
        {

            Rotate();


        }   
        
        
        	
	}
    void Rotate()
    {
        // Set the rotation of model to roation * speed and deltat time.
        modelTransform.Rotate(rotation * speed * Time.deltaTime);

    }


}
