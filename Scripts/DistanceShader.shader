﻿Shader "Unlit/DistanceShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Amount("Amount", float) = 0 

	}
		SubShader{

		Tags { "RenderType" = "Opaque" }

		// turn cull of so we can see backfaces
		Cull Off
		// CGPROGRAM STARTS HERE.
		CGPROGRAM
		// Seting up Prgama references to sur and vert functions.
		// Thease functions will be called every vertex and every pixel.
		#pragma surface surf Lambert vertex:vert 

		struct Input {
			float2 uv_MainTex;
		};
	

		// float that is referincing the properity _Amount
		float _Amount;
		void vert(inout appdata_full v)
		{
			//offset the xys coordiantes for every vertex along their normal direction by the amount property
			v.vertex.xyz += v.normal * _Amount;

		}
		// use this as frag instead of surface i guess
		sampler2D _MainTex;
		fixed4 _Col;
		void surf(Input IN, inout SurfaceOutput o)
		{
			// Set albedo to the input rgb and coordinates , in other words do nothing out of the ordinary
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;


		}
		ENDCG
		}
			Fallback "Diffuse"
}

