﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface InputInterface
{

    void Input(int value);
    void ChangeValuePositive();
    void ChangeValueNegative();
    void ResetValues();
}
