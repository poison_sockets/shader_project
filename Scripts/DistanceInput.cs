﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceInput : InputClass
{


    float valuetoChange = 0;
    float minValue;
    float maxValue;
    float step;

    public DistanceInput (float maxVal, float minVal, float st)
    {
        if (minVal < maxValue)
        {

            minValue = minVal;
            maxValue = maxVal;

        }
        else
        {
            minVal = 0;
            maxVal = 1;


        }
        step = st;

    }


    public float GetChangedValue
    {
        get { return valuetoChange; }
       
    }
    
    public sealed override void ChangeValuePositive()
    {



        if (valuetoChange < maxValue)
        {
            valuetoChange += step;

        }
        else
        {
            valuetoChange = maxValue;
        }




    }
    public sealed override void ChangeValueNegative()
    {
        


        if (valuetoChange > minValue)
        {
            valuetoChange -= step;

        }
        else
        {
            valuetoChange = minValue;
        }




    }


    public override void Input(int value)
    {

        if (value > 0)
        {
            ChangeValuePositive();
        }
        if (value < 0)
        {
            ChangeValueNegative();
        }

    }

    
    public override void ResetValues()
    {
        valuetoChange = 0;

    }


}
