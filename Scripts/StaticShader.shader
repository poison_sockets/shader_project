﻿Shader "Unlit/StaticShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RandomNoiseTexture("Noise Texture",2D) = "white"{}

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

		float rand(float2 co)
		{
			// Random function based off of a Psudo random algorithm found on the internet
			// it uses the frac function to get a floating point based on multiplying and adding numbers with the _Time property 
			// the numbers are also passed through a sin function
			return frac((sin(dot(co.xy, float2(12.345 * _Time.w, 32.59 * _Time.w))) * 12345.67890 + _Time.w));
		}

		// Struct to pass vertex data to the fragment shader.
			struct v2f
			{
				
				float2 uv : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};
			//Parameter References
			sampler2D _MainTex;
			sampler2D _RandomNoiseTexture;
			float4 _MainTex_ST;
			

			// Vertex shader, Not much is done hear but pass the world position onto the fragment shader, the rest is generated code from unity
			v2f vert (appdata v)
			{
				v2f o;
				// setting the output worldposition to the current vertex
				o.worldPosition = v.vertex;

				// standard unlit shader voodoo
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv = v.uv;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			// Dragment shader Called every pixe
			fixed4 frag (v2f i) : SV_Target
			{

				// Looking at a random pixel in the Random noise texture based on time;
				// Is partly responsible for the horizontal lines that run down the object.
				float random = tex2D(_RandomNoiseTexture,float2(i.worldPosition.y += _Time[1]* 10,0)).r;

				// taking the random noise and putting it into a psudo random function
				float noise = rand(random);
				// Interpolating between black and white based on the calculated random noise
				fixed4 col = lerp(fixed4(0,0,0,0),fixed4(1,1,1,1), noise)

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				// Returning the pixel
				return col;
			}
			ENDCG
		}
	}
}
