﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    //Passes on user input to the shader delegate class;


    //creating input classes 
    DistanceInput distInput = new DistanceInput(2f, -2f, 0.1f);
    ToggleInput toggleInput = new ToggleInput();

    public ShaderDelegate shaderDel;

    // Creating delegates that that are used to call functions in the shader delegate script.
    public delegate void DistanceDelegte(float dist);
    public DistanceDelegte disDel;

    public delegate void ToggleDelegate(bool isOn);
    public ToggleDelegate togDel;

    public delegate void ResetDelegate();
    public ResetDelegate resDel;

    // Constant ints for default values
    const int positive = 1;
    const int negative = -1;

	// Use this for initialization
	void Start () {

        shaderDel = gameObject.GetComponent<ShaderDelegate>();
	}
	


	// Update is called once per frame
	void FixedUpdate () {
		
        // If input A or D increase the extrusion value on the extrusion shader
        if(Input.GetKey(KeyCode.D))
        {
            distInput.Input(positive);
            disDel.Invoke(distInput.GetChangedValue);
        }

        if (Input.GetKey(KeyCode.A))
        {
            distInput.Input(negative);
            disDel.Invoke(distInput.GetChangedValue);
        }

        //reset the extrusion value on the extrusion shader to zero
        if (Input.GetKey(KeyCode.R))
        {

            resDel.Invoke();
            distInput.ResetValues();
        }


        //Turn the Full screen shader on and off
        if (Input.GetKey(KeyCode.W))
        {

            toggleInput.Input(positive);
            togDel.Invoke(toggleInput.GetValue());
               
        }
        if (Input.GetKey(KeyCode.S))
        {

            toggleInput.Input(negative);
            togDel.Invoke(toggleInput.GetValue());
        }
  
 
        // if Q turn Extrusion shader on
        // if E turn TV Static shader on
        if (Input.GetKey(KeyCode.Q))
        {
            shaderDel.SwitchShader(true);
        }
        if (Input.GetKey(KeyCode.E))
        {
            shaderDel.SwitchShader(false);

        }


    }
}
