﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputClass : InputInterface {


    public abstract void ChangeValuePositive();

    public abstract void ChangeValueNegative();

    public abstract void Input(int value);

    public abstract void ResetValues();
    
    

}
