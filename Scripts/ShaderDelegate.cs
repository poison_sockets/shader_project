﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderDelegate : MonoBehaviour
{

    public InputHandler inHand;
    public Material DistanceMaterial;


    // References to shaders so we can swap between them during run time.
    public Shader tvStaticShader;
    public Shader normalExtrusionShader;

    public List<Material> matList = new List<Material>();

    // reference to the camera so we can turn the full screen shader on and off;
    public GameObject camerA;
    // Reference to the object, so we can switch shader types
    public GameObject objectToBeShaded;



    // Reference to the shaded object's renderer;
    public Renderer rend;

    float currentDistance;
 

    bool changeShader;

    // Use this for initialization
    void Start()
    {
        //Finding the input Handler function so we can enroll functions in delegates
        inHand = FindObjectOfType<InputHandler>();
        // Enrolling functions in delegates.
        inHand.disDel += UpdateDistance;
        inHand.togDel += ToggleFullScreenShader;
        inHand.resDel += ResetShader;


        changeShader = false;

        rend = objectToBeShaded.GetComponent<Renderer>();


        //Finding the shaders and storing them in varibles at start;
        tvStaticShader = Shader.Find("Unlit/StaticShader");
        normalExtrusionShader = Shader.Find("Unlit/DistanceShader");


    }

    // Update is called once per frame


    void UpdateDistance(float dist)
    {

        // Check to see if current shader is the extrusion shader if so change it's value else do nothing 
        if(!changeShader)
        {

            DistanceMaterial.SetFloat("_Amount", dist);

        }
 

    }
    void ToggleFullScreenShader(bool IsOn)
    {

        camerA.GetComponent<ApplyImageEffect>().Apply = IsOn;
        //Turn Full Screen Shader on off
    }
    void ResetShader()
    {
        // Function to reset the _Amount on the Extrusion shader.
        DistanceMaterial.SetFloat("_Amount", 0f);


    }
    //function to switch shaders and materials on the object.
  public  void SwitchShader(bool switchShader)
    {

        changeShader = switchShader; 
        if(!changeShader)
        {
            rend.material.shader = normalExtrusionShader;
            rend.material = matList[0];
        }
        else
        {
            rend.material.shader = tvStaticShader;
            rend.material = matList[1];
        }


    }

}
